resource "aws_iam_group_membership" "squad" {
  name = "tf-testing-group-membership"

 users = [
    aws_iam_user.mayur.name,
  ]
  group = aws_iam_group.mayur-grp.name

}

resource "aws_iam_group" "mayur-grp" {
  name = "mygrp"
}


resource "aws_iam_user" "mayur" {
  name = "mayur-user"
  tags = {
    tag-key = "tag11"
  }
}


